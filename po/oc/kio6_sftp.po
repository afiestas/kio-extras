# translation of kio_sftp.po to Occitan (lengadocian)
# Occitan translation of kio_sftp.po
# Copyright (C) 2003, 2004, 2007, 2008 Free Software Foundation, Inc.
#
# Yannig MARCHEGAY (Kokoyaya) <yannig@marchegay.org> - 2006-2007
#
# Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kio_sftp\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-05 00:37+0000\n"
"PO-Revision-Date: 2008-08-05 22:26+0200\n"
"Last-Translator: Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>\n"
"Language-Team: Occitan (lengadocian) <ubuntu-l10n-oci@lists.ubuntu.com>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: KBabel 1.11.4\n"

#: kio_sftp.cpp:262
#, kde-format
msgid "Incorrect or invalid passphrase"
msgstr ""

#: kio_sftp.cpp:311
#, kde-format
msgid "Could not allocate callbacks"
msgstr ""

#: kio_sftp.cpp:324
#, kde-format
msgid "Could not set log verbosity."
msgstr ""

#: kio_sftp.cpp:329
#, kde-format
msgid "Could not set log userdata."
msgstr ""

#: kio_sftp.cpp:334
#, kde-format
msgid "Could not set log callback."
msgstr ""

#: kio_sftp.cpp:370 kio_sftp.cpp:372 kio_sftp.cpp:883
#, kde-format
msgid "SFTP Login"
msgstr ""

#: kio_sftp.cpp:387
#, kde-format
msgid "Use the username input field to answer this question."
msgstr ""

#: kio_sftp.cpp:400
#, kde-format
msgid "Please enter your password."
msgstr ""

#: kio_sftp.cpp:405 kio_sftp.cpp:886
#, kde-format
msgid "Site:"
msgstr ""

#: kio_sftp.cpp:450
#, kde-format
msgctxt "error message. %1 is a path, %2 is a numeric error code"
msgid "Could not read link: %1 [%2]"
msgstr ""

#: kio_sftp.cpp:570
#, kde-format
msgid "Could not create a new SSH session."
msgstr ""

#: kio_sftp.cpp:581 kio_sftp.cpp:585
#, kde-format
msgid "Could not set a timeout."
msgstr ""

#: kio_sftp.cpp:592
#, kde-format
msgid "Could not disable Nagle's Algorithm."
msgstr ""

#: kio_sftp.cpp:598 kio_sftp.cpp:603
#, kde-format
msgid "Could not set compression."
msgstr ""

#: kio_sftp.cpp:609
#, kde-format
msgid "Could not set host."
msgstr ""

#: kio_sftp.cpp:615
#, kde-format
msgid "Could not set port."
msgstr ""

#: kio_sftp.cpp:623
#, kde-format
msgid "Could not set username."
msgstr ""

#: kio_sftp.cpp:630
#, kde-format
msgid "Could not parse the config file."
msgstr ""

#: kio_sftp.cpp:645
#, kde-kuit-format
msgid "Opening SFTP connection to host %1:%2"
msgstr ""

#: kio_sftp.cpp:685
#, kde-format
msgid "Could not get server public key type name"
msgstr ""

#: kio_sftp.cpp:697
#, kde-format
msgid "Could not create hash from server public key"
msgstr ""

#: kio_sftp.cpp:706
#, kde-format
msgid "Could not create fingerprint for server public key"
msgstr ""

#: kio_sftp.cpp:765
#, kde-format
msgid ""
"An %1 host key for this server was not found, but another type of key "
"exists.\n"
"An attacker might change the default server key to confuse your client into "
"thinking the key does not exist.\n"
"Please contact your system administrator.\n"
"%2"
msgstr ""

#: kio_sftp.cpp:782
#, kde-format
msgctxt "@title:window"
msgid "Host Identity Change"
msgstr ""

#: kio_sftp.cpp:784
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>The host key for the server <emphasis>%1</emphasis> has changed.</"
"para><para>This could either mean that DNS spoofing is happening or the IP "
"address for the host and its host key have changed at the same time.</"
"para><para>The %2 key fingerprint sent by the remote host is:<bcode>%3</"
"bcode>Are you sure you want to continue connecting?</para>"
msgstr ""

#: kio_sftp.cpp:794
#, kde-format
msgctxt "@title:window"
msgid "Host Verification Failure"
msgstr ""

#: kio_sftp.cpp:796
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>The authenticity of host <emphasis>%1</emphasis> cannot be established."
"</para><para>The %2 key fingerprint is:<bcode>%3</bcode>Are you sure you "
"want to continue connecting?</para>"
msgstr ""

#: kio_sftp.cpp:805
#, kde-format
msgctxt "@action:button"
msgid "Connect Anyway"
msgstr ""

#: kio_sftp.cpp:828 kio_sftp.cpp:847 kio_sftp.cpp:862 kio_sftp.cpp:875
#: kio_sftp.cpp:927 kio_sftp.cpp:937
#, kde-format
msgid "Authentication failed."
msgstr ""

#: kio_sftp.cpp:835
#, kde-format
msgid ""
"Authentication failed. The server didn't send any authentication methods"
msgstr ""

#: kio_sftp.cpp:884
#, kde-format
msgid "Please enter your username and password."
msgstr ""

#: kio_sftp.cpp:895
#, kde-format
msgid "Incorrect username or password"
msgstr ""

#: kio_sftp.cpp:944
#, kde-format
msgid ""
"Unable to request the SFTP subsystem. Make sure SFTP is enabled on the "
"server."
msgstr ""

#: kio_sftp.cpp:949
#, kde-format
msgid "Could not initialize the SFTP session."
msgstr ""

#: kio_sftp.cpp:953
#, kde-format
msgid "Successfully connected to %1"
msgstr ""

#: kio_sftp.cpp:1006
#, kde-format
msgid "Invalid sftp context"
msgstr ""

#: kio_sftp.cpp:1538
#, kde-format
msgid ""
"Could not change permissions for\n"
"%1"
msgstr ""

#~ msgid "Connection failed."
#~ msgstr "Error de connexion."
