# translation of kio_man.po to Kaszëbsczi
#
# Michôł Òstrowsczi <michol@linuxcsb.org>, 2006, 2007, 2008.
# Mark Kwidzińsczi <mark@linuxcsb.org>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kio_man\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-02 01:39+0000\n"
"PO-Revision-Date: 2008-12-11 22:28+0100\n"
"Last-Translator: Mark Kwidzińsczi <mark@linuxcsb.org>\n"
"Language-Team: Kashubian <i18n-csb@linuxcsb.org>\n"
"Language: csb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.2\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2)\n"

#: kio_man.cpp:461
#, fuzzy, kde-kuit-format
#| msgid ""
#| "No man page matching to %1 found.<br /><br />Check that you have not "
#| "mistyped the name of the page that you want.<br />Check that you have "
#| "typed the name using the correct upper and lower case characters.<br />If "
#| "everything looks correct, then you may need to improve the search path "
#| "for man pages; either using the environment variable MANPATH or using a "
#| "matching file in the /etc directory."
msgctxt "@info"
msgid ""
"No man page matching <resource>%1</resource> could be found.<nl/><nl/>Check "
"that you have not mistyped the name of the page, and note that man page "
"names are case sensitive.<nl/><nl/>If the name is correct, then you may need "
"to extend the search path for man pages, either using the <envar>MANPATH</"
"envar> environment variable or a configuration file in the <filename>/etc</"
"filename> directory."
msgstr ""
"Nie je nalazłô niżódnô starna pòmòcë dlô %1.<br /><br />Proszã sprôwdzëc, "
"czë miono szëkóny starnë òstało wpisóné bezzmiłkòwò. <br />Bôczënk - wôlgòsc "
"lëterów je wôżnô!<br />Jeżle wszëtkò wëzdrzi fejn, to mòże nót je rozcygnąc "
"lëstã przëszëkiwónëch stegnów: abò ùstawiając zmienną MANPATH abò editëjąc "
"gwësny lopk w katalogù /etc ."

#: kio_man.cpp:575
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The specified man page references another page <filename>%1</filename>,<nl/"
">but the referenced page <filename>%2</filename> could not be found."
msgstr ""

#: kio_man.cpp:592
#, kde-kuit-format
msgctxt "@info"
msgid "The man page <filename>%1</filename> could not be read."
msgstr ""

#: kio_man.cpp:601
#, kde-kuit-format
msgctxt "@info"
msgid "The man page <filename>%1</filename> could not be converted."
msgstr ""

#: kio_man.cpp:663
#, fuzzy, kde-format
#| msgid "<h1>KDE Man Viewer Error</h1>"
msgid "Manual Page Viewer Error"
msgstr "<h1>Fela przezérnika KDE Man</h1>"

#: kio_man.cpp:676
#, fuzzy, kde-format
#| msgid "There is more than one matching man page."
msgid "There is more than one matching man page:"
msgstr "Pasëjë czile starnów ùczbòwnika."

#: kio_man.cpp:676
#, kde-format
msgid "Multiple Manual Pages"
msgstr ""

#: kio_man.cpp:689
#, kde-format
msgid ""
"Note: if you read a man page in your language, be aware it can contain some "
"mistakes or be obsolete. In case of doubt, you should have a look at the "
"English version."
msgstr ""
"Bôczënk: starna ùczbòwnika napisóna w Twòjim jãzëkù mòże nie bëc aktualna "
"abò miec felë. Eżle môsz jaczé trëchlënë, téj zazdrzë do anielsczi wersëji."

#: kio_man.cpp:757
#, kde-format
msgid "Header Files"
msgstr ""

#: kio_man.cpp:759
#, kde-format
msgid "Header Files (POSIX)"
msgstr ""

#: kio_man.cpp:761
#, kde-format
msgid "User Commands"
msgstr "Pòlét brëkòwnika"

#: kio_man.cpp:763
#, fuzzy, kde-format
#| msgid "User Commands"
msgid "User Commands (POSIX)"
msgstr "Pòlét brëkòwnika"

#: kio_man.cpp:765
#, kde-format
msgid "System Calls"
msgstr "Systemòwé fùnkcje"

#: kio_man.cpp:767
#, kde-format
msgid "Subroutines"
msgstr "Pòdprogramë"

#: kio_man.cpp:770
#, kde-format
msgid "Perl Modules"
msgstr "Mòdułë Perla"

#: kio_man.cpp:772
#, kde-format
msgid "Network Functions"
msgstr "Sécowé fùnkcje"

#: kio_man.cpp:774
#, kde-format
msgid "Devices"
msgstr "Ùrządzenia"

#: kio_man.cpp:776
#, kde-format
msgid "File Formats"
msgstr "Fòrmatë lopków"

#: kio_man.cpp:778
#, kde-format
msgid "Games"
msgstr "Grë"

#: kio_man.cpp:780
#, kde-format
msgid "Miscellaneous"
msgstr "Różné"

#: kio_man.cpp:782
#, kde-format
msgid "System Administration"
msgstr "Sprôwanié systemù"

#: kio_man.cpp:784
#, kde-format
msgid "Kernel"
msgstr "Jądro"

#: kio_man.cpp:786
#, kde-format
msgid "Local Documentation"
msgstr "Môlowô dokùmentacëja"

#: kio_man.cpp:789
#, kde-format
msgid "New"
msgstr "Nowi"

#: kio_man.cpp:816
#, fuzzy, kde-format
#| msgid "UNIX Manual Index"
msgid "Main Manual Page Index"
msgstr "Indeks ùczbòwnika UNIX"

#: kio_man.cpp:841
#, kde-format
msgid "Section %1"
msgstr "Dzél  %1"

#: kio_man.cpp:1090
#, fuzzy, kde-format
#| msgid "Index for Section %1: %2"
msgid "Index for section %1: %2"
msgstr "Spisënk zamkłoscë dzélów %1: %2"

#: kio_man.cpp:1090
#, fuzzy, kde-format
#| msgid "UNIX Manual Index"
msgid "Manual Page Index"
msgstr "Indeks ùczbòwnika UNIX"

#: kio_man.cpp:1102
#, kde-format
msgid "Generating Index"
msgstr "Robienie spisu zamkłoscë"

#: kio_man.cpp:1356
#, fuzzy, kde-kuit-format
#| msgid ""
#| "Could not find the sgml2roff program on your system. Please install it, "
#| "if necessary, and extend the search path by adjusting the environment "
#| "variable PATH before starting KDE."
msgctxt "@info"
msgid ""
"Could not find the <command>%1</command> program on your system. Please "
"install it if necessary, and ensure that it can be found using the "
"environment variable <envar>PATH</envar>."
msgstr ""
"Nie mòże nalezc programë sgml2roff. Proszã gò zainstalowac abò pòprawic "
"nastôwë stegnë, ùstawiając zmienną PATH przed zrëszënim KDE."

#~ msgid "Open of %1 failed."
#~ msgstr "Nie mòże òtemknąc %1."

#~ msgid "Man output"
#~ msgstr "Zamkłosc ùczbòwnika"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Mark Kwidzińsczi, Michôł Òstrowsczi"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mark@linuxcsb.org, michol@linuxcsb.org"

#~ msgid "KMan"
#~ msgstr "KMan"
