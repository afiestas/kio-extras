# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2015, 2018, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-05 00:37+0000\n"
"PO-Revision-Date: 2022-08-03 10:07+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: kio_sftp.cpp:262
#, kde-format
msgid "Incorrect or invalid passphrase"
msgstr "Felaktig eller ogiltig lösenordsfras"

#: kio_sftp.cpp:311
#, kde-format
msgid "Could not allocate callbacks"
msgstr "Kunde inte reservera återanrop"

#: kio_sftp.cpp:324
#, kde-format
msgid "Could not set log verbosity."
msgstr "Kunde inte ställa in loggens detaljnivå"

#: kio_sftp.cpp:329
#, kde-format
msgid "Could not set log userdata."
msgstr "Kunde inte ställa in loggens användardata."

#: kio_sftp.cpp:334
#, kde-format
msgid "Could not set log callback."
msgstr "Kunde inte ställa in loggens återanrop."

#: kio_sftp.cpp:370 kio_sftp.cpp:372 kio_sftp.cpp:883
#, kde-format
msgid "SFTP Login"
msgstr "SFTP-inloggning"

#: kio_sftp.cpp:387
#, kde-format
msgid "Use the username input field to answer this question."
msgstr "Använd indatafältet användarnamn för att besvara frågan."

#: kio_sftp.cpp:400
#, kde-format
msgid "Please enter your password."
msgstr "Ange ditt lösenord."

#: kio_sftp.cpp:405 kio_sftp.cpp:886
#, kde-format
msgid "Site:"
msgstr "Plats:"

#: kio_sftp.cpp:450
#, kde-format
msgctxt "error message. %1 is a path, %2 is a numeric error code"
msgid "Could not read link: %1 [%2]"
msgstr "Kunde inte läsa länken: %1 [%2]"

#: kio_sftp.cpp:570
#, kde-format
msgid "Could not create a new SSH session."
msgstr "Kunde inte skapa ny SSH-session."

#: kio_sftp.cpp:581 kio_sftp.cpp:585
#, kde-format
msgid "Could not set a timeout."
msgstr "Kunde inte ställa in en tidsgräns."

#: kio_sftp.cpp:592
#, kde-format
msgid "Could not disable Nagle's Algorithm."
msgstr "Kunde inte inaktivera Nagles algoritm."

#: kio_sftp.cpp:598 kio_sftp.cpp:603
#, kde-format
msgid "Could not set compression."
msgstr "Kunde inte ställa in komprimering."

#: kio_sftp.cpp:609
#, kde-format
msgid "Could not set host."
msgstr "Kunde inte ställa in värddator."

#: kio_sftp.cpp:615
#, kde-format
msgid "Could not set port."
msgstr "Kunde inte ställa in port."

#: kio_sftp.cpp:623
#, kde-format
msgid "Could not set username."
msgstr "Kunde inte ställa in användarnamn."

#: kio_sftp.cpp:630
#, kde-format
msgid "Could not parse the config file."
msgstr "Kunde inte tolka inställningsfilen."

#: kio_sftp.cpp:645
#, kde-kuit-format
msgid "Opening SFTP connection to host %1:%2"
msgstr "Öppnar SFTP-anslutning till värddator %1:%2"

#: kio_sftp.cpp:685
#, kde-format
msgid "Could not get server public key type name"
msgstr "Kunde inte hämta typnamn på serverns öppna nyckel"

#: kio_sftp.cpp:697
#, kde-format
msgid "Could not create hash from server public key"
msgstr "Kunde inte skapa kondensat från serverns öppna nyckel"

#: kio_sftp.cpp:706
#, kde-format
msgid "Could not create fingerprint for server public key"
msgstr "Kunde inte skapa fingeravtryck för serverns öppna nyckel"

#: kio_sftp.cpp:765
#, kde-format
msgid ""
"An %1 host key for this server was not found, but another type of key "
"exists.\n"
"An attacker might change the default server key to confuse your client into "
"thinking the key does not exist.\n"
"Please contact your system administrator.\n"
"%2"
msgstr ""
"Värddatornyckeln %1 för servern hittades inte, men en annan nyckeltyp "
"finns.\n"
"En attack kan ändra serverns standardnyckel för att förvirra klienter så att "
"de tror att nyckeln inte finns.\n"
"Kontakta systemadministratören.\n"
"%2"

#: kio_sftp.cpp:782
#, kde-format
msgctxt "@title:window"
msgid "Host Identity Change"
msgstr "Värddatorns identitet ändrad"

#: kio_sftp.cpp:784
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>The host key for the server <emphasis>%1</emphasis> has changed.</"
"para><para>This could either mean that DNS spoofing is happening or the IP "
"address for the host and its host key have changed at the same time.</"
"para><para>The %2 key fingerprint sent by the remote host is:<bcode>%3</"
"bcode>Are you sure you want to continue connecting?</para>"
msgstr ""
"<para>Värddatornyckeln för servern <emphasis>%1</emphasis> har ändrats.</"
"para><para>Det kan antingen betyda att DNS-förfalskning har inträffat eller "
"att värddatorns IP-adress och dess värddatornyckel har ändrats samtidigt.</"
"para><para>Fingeravtryck för nyckeln %2 som skickas av fjärrdatorn är:<bcode>"
"%3</bcode>Är du säker på att du vill fortsätta ansluta?</para>"

#: kio_sftp.cpp:794
#, kde-format
msgctxt "@title:window"
msgid "Host Verification Failure"
msgstr "Värddatorverifiering misslyckades"

#: kio_sftp.cpp:796
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<para>The authenticity of host <emphasis>%1</emphasis> cannot be established."
"</para><para>The %2 key fingerprint is:<bcode>%3</bcode>Are you sure you "
"want to continue connecting?</para>"
msgstr ""
"<para>Äkthet för värddatorn <emphasis>%1</emphasis> kan inte fastställas.</"
"para><para>Fingeravtryck för nyckeln %2 är: <bcode>%3</bcode>Är du säker på "
"att du vill fortsätta ansluta?</para>"

#: kio_sftp.cpp:805
#, kde-format
msgctxt "@action:button"
msgid "Connect Anyway"
msgstr "Anslut ändå"

#: kio_sftp.cpp:828 kio_sftp.cpp:847 kio_sftp.cpp:862 kio_sftp.cpp:875
#: kio_sftp.cpp:927 kio_sftp.cpp:937
#, kde-format
msgid "Authentication failed."
msgstr "Autentisering misslyckades."

#: kio_sftp.cpp:835
#, kde-format
msgid ""
"Authentication failed. The server didn't send any authentication methods"
msgstr ""
"Behörighetskontroll misslyckades. Servern skickade inte några "
"behörighetskontrollmetoder."

#: kio_sftp.cpp:884
#, kde-format
msgid "Please enter your username and password."
msgstr "Ange ditt användarnamn och lösenord."

#: kio_sftp.cpp:895
#, kde-format
msgid "Incorrect username or password"
msgstr "Felaktigt användarnamn eller lösenord"

#: kio_sftp.cpp:944
#, kde-format
msgid ""
"Unable to request the SFTP subsystem. Make sure SFTP is enabled on the "
"server."
msgstr ""
"Kunde inte begära SFTP-delsystemet. Försäkra dig om att SFTP är aktiverat på "
"servern."

#: kio_sftp.cpp:949
#, kde-format
msgid "Could not initialize the SFTP session."
msgstr "Kunde inte initiera SFTP-sessionen."

#: kio_sftp.cpp:953
#, kde-format
msgid "Successfully connected to %1"
msgstr "Ansluten till %1"

#: kio_sftp.cpp:1006
#, kde-format
msgid "Invalid sftp context"
msgstr "Ogiltigt sftp-sammanhang"

#: kio_sftp.cpp:1538
#, kde-format
msgid ""
"Could not change permissions for\n"
"%1"
msgstr ""
"Kunde inte ändra rättigheter för\n"
"%1"

#~ msgid ""
#~ "The host key for the server %1 has changed.\n"
#~ "This could either mean that DNS SPOOFING is happening or the IP address "
#~ "for the host and its host key have changed at the same time.\n"
#~ "The fingerprint for the %2 key sent by the remote host is:\n"
#~ "  SHA256:%3\n"
#~ "Please contact your system administrator.\n"
#~ "%4"
#~ msgstr ""
#~ "Värddatornyckeln för servern %1 har ändrats.\n"
#~ "Det kan antingen betyda att DNS-förfalskning har inträffat eller att "
#~ "värddatorns IP-adress och dess värddatornyckel har ändrats samtidigt.\n"
#~ "Fingeravtrycket för nyckeln %2 som skickas av fjärrdatorn är:\n"
#~ "  SHA256: %3\n"
#~ "Kontakta systemadministratören.\n"
#~ "%4"

#~ msgid "Warning: Cannot verify host's identity."
#~ msgstr "Varning: Kan inte verifiera värddatorns identitet."

#~ msgid ""
#~ "The host key for this server was not found, but another type of key "
#~ "exists.\n"
#~ "An attacker might change the default server key to confuse your client "
#~ "into thinking the key does not exist.\n"
#~ "Please contact your system administrator.\n"
#~ "%1"
#~ msgstr ""
#~ "Värddatornyckeln för servern hittades inte, men en annan nyckeltyp "
#~ "finns.\n"
#~ "En attack kan ändra serverns standardnyckel för att förvirra klienter så "
#~ "att de tror att nyckeln inte finns.\n"
#~ "Kontakta systemadministratören.\n"
#~ "%1"

#~ msgid ""
#~ "The authenticity of host %1 cannot be established.\n"
#~ "The key fingerprint is: %2\n"
#~ "Are you sure you want to continue connecting?"
#~ msgstr ""
#~ "Äkthet för värddatorn %1 kan inte fastställas.\n"
#~ "Nyckelns fingeravtryck är: %2\n"
#~ "Är du säker på att du vill fortsätta ansluta?"
