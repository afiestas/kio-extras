# translation of kio_nfs.po to Romanian
# translation of @PACKAGE.po to @LANGUAGE
# Copyright (C) 2004 Free Software Foundation, Inc.
# Claudiu Costin <claudiuc@kde.org>, 2003, 2004.
# Sergiu Bivol <sergiu@cip.md>, 2008, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: kio_nfs\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-11 02:01+0000\n"
"PO-Revision-Date: 2021-04-16 17:33+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 19.12.3\n"

#: kio_nfs.cpp:153
#, kde-format
msgid "Cannot find an NFS version that host '%1' supports"
msgstr "Nu se poate găsi o versiune NFS susținută de gazda „%1”"

#: kio_nfs.cpp:323
#, kde-format
msgid "The NFS protocol requires a server host name."
msgstr "Protocolul NFS necesită o denumire de gazdă pentru server."

#: kio_nfs.cpp:353
#, kde-format
msgid "Failed to initialise protocol"
msgstr "Inițializarea protocolului a eșuat"

#: kio_nfs.cpp:821
#, kde-format
msgid "RPC error %1, %2"
msgstr "Eroare RPC %1, %2"

#: kio_nfs.cpp:870
#, kde-format
msgid "Filename too long"
msgstr "Denumire fișier prea lungă"

#: kio_nfs.cpp:877
#, kde-format
msgid "Disk quota exceeded"
msgstr "Cotă de disc depășită"

#: kio_nfs.cpp:883
#, kde-format
msgid "NFS error %1, %2"
msgstr "Eroare NFS %1, %2"

#: nfsv2.cpp:392 nfsv2.cpp:443 nfsv3.cpp:430 nfsv3.cpp:574
#, kde-format
msgid "Unknown target"
msgstr "Țintă necunoscută"

#~ msgid "No space left on device"
#~ msgstr "Nu mai există spațiu de disc"

#~ msgid "Read only file system"
#~ msgstr "Sistem de fișiere numai în citire"

#~ msgid "An RPC error occurred."
#~ msgstr "A apărut o eroare RPC."
