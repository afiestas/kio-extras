# translation of kio_nfs.po to Gujarati
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sweta Kothari <swkothar@redhat.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kio_nfs\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-11 02:01+0000\n"
"PO-Revision-Date: 2008-07-08 15:31+0530\n"
"Last-Translator: Sweta Kothari <swkothar@redhat.com>\n"
"Language-Team: Gujarati\n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#: kio_nfs.cpp:153
#, kde-format
msgid "Cannot find an NFS version that host '%1' supports"
msgstr ""

#: kio_nfs.cpp:323
#, kde-format
msgid "The NFS protocol requires a server host name."
msgstr ""

#: kio_nfs.cpp:353
#, kde-format
msgid "Failed to initialise protocol"
msgstr ""

#: kio_nfs.cpp:821
#, kde-format
msgid "RPC error %1, %2"
msgstr ""

#: kio_nfs.cpp:870
#, kde-format
msgid "Filename too long"
msgstr "ફાઇલનું નામ ખૂબ લાંબુ છે"

#: kio_nfs.cpp:877
#, kde-format
msgid "Disk quota exceeded"
msgstr "Disk quota ખૂબ વધારે છે"

#: kio_nfs.cpp:883
#, kde-format
msgid "NFS error %1, %2"
msgstr ""

#: nfsv2.cpp:392 nfsv2.cpp:443 nfsv3.cpp:430 nfsv3.cpp:574
#, kde-format
msgid "Unknown target"
msgstr ""

#~ msgid "No space left on device"
#~ msgstr "ઉપકરણ પર જગ્યા બાકી નથી"

#~ msgid "Read only file system"
#~ msgstr "ફક્ત ફાઇલ સિસ્ટમ વાંચો"

#~ msgid "An RPC error occurred."
#~ msgstr "RPC ભૂલ ઉત્પન્ન થયેલ છે."
